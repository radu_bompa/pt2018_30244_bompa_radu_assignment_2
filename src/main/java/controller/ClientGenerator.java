package controller;

import entities.*;
import entities.Queue;
import view.MainWindow;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * Created by Radu Bompa
 * 4/4/2018
 */
public class ClientGenerator extends Observable implements Runnable {
	private int clientId = 1;
	private int minArrTime;
	private int maxArrTime;
	private int minSerTime;
	private int maxSerTime;
	private int nrQueues;
	private int simDuration;
	public static boolean simRunning = false;

	private ScheduledThreadPoolExecutor exec;

	private static int simId = 0;

	private List<Queue> queues = new LinkedList<>();

	public void generate(int minArrivalTime, int maxArrivalTime, int minServiceTime, int maxServiceTime,
	                     int noOfQueues, int simulationDuration) throws InterruptedException {
		minArrTime = minArrivalTime;
		maxArrTime = maxArrivalTime;
		minSerTime = minServiceTime;
		maxSerTime = maxServiceTime;
		nrQueues = noOfQueues;
		simDuration = simulationDuration;

		addObserver(new MainWindow());
		// pause to avoid churning
		exec = new ScheduledThreadPoolExecutor(noOfQueues + 1);

		exec.schedule(this, 100, TimeUnit.MILLISECONDS);
		invalidateQueues();
		
		for(int i = 0; i < noOfQueues; i++) {
			Queue queue = new Queue(minServiceTime, maxServiceTime);
			queue.setQueueId(i);
			queue.setWaitingClients(new LinkedList<>());
			queue.setQueueAvailable(true);
			exec.schedule(queue, 200, TimeUnit.MILLISECONDS);
			queues.add(queue);
		}
		simId++;

	}


	@Override
	public void run() {
		simRunning = true;
		int threadId = simId;
		long t = System.currentTimeMillis();
		long end = t + simDuration * 1000;
		Map<Integer, Client> waitingClients = new LinkedHashMap<>();
		List<Client> finishedClients = new LinkedList<>();
		while (System.currentTimeMillis() < end && threadId == simId) {
			MainWindow.createClient(clientId);
			Client newClient = new Client();
			newClient.setId(clientId++);
			newClient.setArrivalTime(new Date());
			newClient.setMinimumServiceTime(minSerTime);
			newClient.setMaximumServiceTime(maxSerTime);
			newClient.setStatus(Client.ClientStatus.ARRIVED);

			try {
				Logger.log("Client " + newClient.getId() + " arrived at " + newClient.getArrivalTime() + "\n");
			} catch (IOException e) {
				e.printStackTrace();
			}

			waitingClients.put(newClient.getId(), newClient);

			Queue q = addClientToQueue(newClient);
			newClient.setQueueId(q.getQueueId());
			newClient.setWaiting(true);
			setChanged();
			notifyObservers(newClient);
			try {
				Thread.sleep(ThreadLocalRandom.current().nextInt(minArrTime, maxArrTime + 1) * 1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		simRunning = false;
	}

	private void invalidateQueues() {
		for (Queue queue : queues) {
			queue.setQueueAvailable(false);
		}
	}

	public Queue addClientToQueue(Client client) {
		entities.Queue leastBusyQueue = getLeastBusyQueue();
		if (leastBusyQueue != null) {
			client.setStatus(Client.ClientStatus.WAITING_IN_LINE);
			leastBusyQueue.getWaitingClients().add(client);
			try {
				Logger.log("Clientul " + client.getId() + " s-a alăturat cozii Queue " + (leastBusyQueue.getQueueId() + 1) + "\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return leastBusyQueue;
	}


	public Queue getLeastBusyQueue() {
		int smallestWaitingTime = Integer.MAX_VALUE;
		Queue queue = null;
		for (Queue q : queues) {
			int queueWaitingTime = 0;
			for (Client client : q.getWaitingClients()) {
				queueWaitingTime += client.getMaximumServiceTime();
			}
			if (queueWaitingTime < smallestWaitingTime) {
				smallestWaitingTime = queueWaitingTime;
				queue = q;
			}
		}
		return queue;
	}
}