package controller;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Radu Bompa
 * 5/2/2018
 */
public class Logger {
	public static void log(String text) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter("simulare_log.txt", true));
		writer.append(text);

		writer.close();
	}
}
