package entities;

import controller.ClientGenerator;
import view.MainWindow;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class Queue extends Observable implements Runnable {
	private int queueId;
	private List<Client> waitingClients;
	private boolean queueAvailable = true;

	private int minServingTime;
	private int maxServingTime;

	public int getQueueId() {
		return queueId;
	}

	public void setQueueId(int queueId) {
		this.queueId = queueId;
	}

	public List<Client> getWaitingClients() {
		return waitingClients;
	}

	public void setWaitingClients(List<Client> waitingClients) {
		this.waitingClients = waitingClients;
	}

	public List<Client> addWaitingClient() {
		return null;
	}

	private void removeWaitingClient(Client client) {
		List<Client> clients = new LinkedList<>();
		for (Client c : waitingClients) {
			if (!Objects.equals(c.getId(), client.getId())) {
				clients.add(c);
			}
		}
		waitingClients = clients;
		client.setStatus(Client.ClientStatus.SERVED);
		client.setWaiting(false);
		client.setLeavingTime(new Date());
		setChanged();
		notifyObservers(client);
	}

	public Queue(int minServingTime, int maxServingTime) {
		this.minServingTime = minServingTime;
		this.maxServingTime = maxServingTime;

		addObserver(new MainWindow());
	}

	public Queue() {

		addObserver(new MainWindow());
	}

	@Override
	public void run() {
//		while simulation is running, check if there are any clients in the queue, and offer services to them, and remove them from the queue
		while (queueAvailable && (ClientGenerator.simRunning || waitingClients.size() > 0)) {
			try {
				if (!serveClient()) {
					Thread.sleep(200);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private boolean serveClient() throws InterruptedException {
		Client client = findFirstClient();
		if (client != null) {
			Thread.sleep(ThreadLocalRandom.current().nextInt(minServingTime, maxServingTime + 1) * 1000);
			removeWaitingClient(client);
			return true;
		}
		return false;
	}

	private Client findFirstClient() {
		Client client = null;
		for (Client c : waitingClients) {
			if (client == null || c.getId() < client.getId()) {
				client = c;
			}
		}
		return client;
	}

	public boolean isQueueAvailable() {
		return queueAvailable;
	}

	public void setQueueAvailable(boolean queueAvailable) {
		this.queueAvailable = queueAvailable;
	}
}
