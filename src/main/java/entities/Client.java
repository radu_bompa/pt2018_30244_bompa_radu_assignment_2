package entities;

import javafx.beans.Observable;

import java.util.Date;

public class Client {
    private Integer id;
    private Integer minimumServiceTime;
    private Integer maximumServiceTime;
    private Date arrivalTime;
    private Date leavingTime;
    private ClientStatus status;

    public enum ClientStatus {
        ARRIVED, WAITING_IN_LINE, SERVED
    }

    private boolean waiting = false;

    private int queueId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMinimumServiceTime() {
        return minimumServiceTime;
    }

    public void setMinimumServiceTime(Integer minimumServiceTime) {
        this.minimumServiceTime = minimumServiceTime;
    }

    public Integer getMaximumServiceTime() {
        return maximumServiceTime;
    }

    public void setMaximumServiceTime(Integer maximumServiceTime) {
        this.maximumServiceTime = maximumServiceTime;
    }

    public Date getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Date getLeavingTime() {
        return leavingTime;
    }

    public void setLeavingTime(Date leavingTime) {
        this.leavingTime = leavingTime;
    }

    public boolean isWaiting() {
        return waiting;
    }

    public void setWaiting(boolean waiting) {
        this.waiting = waiting;
    }

    public int getQueueId() {
        return queueId;
    }

    public void setQueueId(int queueId) {
        this.queueId = queueId;
    }

    public ClientStatus getStatus() {
        return status;
    }

    public void setStatus(ClientStatus status) {
        this.status = status;
    }
}