package view;

import controller.ClientGenerator;
import controller.Logger;
import entities.Client;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;

public class MainWindow extends JPanel implements Observer {
    protected static final JFrame f = new JFrame();//fereastra principala

    private int minArrivalTime;
    private int maxArrivalTime;
    private int minServiceTime;
    private int maxServiceTime;
    private int noOfQueues;
    private int simulationDuration;

    private int removedClients = 0;
    private double totalWaitingTime = 0.00;

    private static JPanel simPanel;

    public MainWindow() {
        super();

        JComponent panel = new JPanel();
        panel.setAlignmentX(JPanel.LEFT_ALIGNMENT);
        panel.setAlignmentY(JPanel.TOP_ALIGNMENT);
        panel.setLayout(null);
        panel.setBounds(0, 0, 300, 600);

        Container pane = f.getContentPane();
        pane.add(panel);
        final GridBagConstraints c = new GridBagConstraints();

//        minimum and maximum interval of arriving time between customers
        JLabel minArrivingIntervalLabel = new JLabel("Min. interval of arriving");
        JTextField minArrivingIntervalText = new JTextField();
        minArrivingIntervalText.setColumns(10);

        JLabel maxArrivingIntervalLabel = new JLabel("Max. interval of arriving");
        JTextField maxArrivingIntervalText = new JTextField();
        maxArrivingIntervalText.setColumns(10);

        minArrivingIntervalLabel.setBounds(0, 30, 170, 20);
        panel.add(minArrivingIntervalLabel);

        minArrivingIntervalText.setBounds(0, 55, 100, 20);
        panel.add(minArrivingIntervalText);

        maxArrivingIntervalLabel.setBounds(0, 80, 170, 20);
        panel.add(maxArrivingIntervalLabel);

        maxArrivingIntervalText.setBounds(0, 105, 100, 20);
        panel.add(maxArrivingIntervalText);

//        Minimum and maximum service time
        JLabel minServiceTimeLabel = new JLabel("Min. service time");
        JTextField minServiceTimeText = new JTextField();
        minServiceTimeText.setColumns(10);
        JLabel maxServiceTimeLabel = new JLabel("Max. service time");
        JTextField maxServiceTimeText = new JTextField();
        maxServiceTimeText.setColumns(10);

        minServiceTimeLabel.setBounds(0, 130, 170, 20);
        panel.add(minServiceTimeLabel);
        minServiceTimeText.setBounds(0, 155, 100, 20);
        panel.add(minServiceTimeText);
        maxServiceTimeLabel.setBounds(0, 180, 170, 20);
        panel.add(maxServiceTimeLabel);
        maxServiceTimeText.setBounds(0, 205, 100, 20);
        panel.add(maxServiceTimeText);

//        Number of queues
        JLabel noOfQueuesLabel = new JLabel("No. of queues");
        JTextField noOfQueuesText = new JTextField();
        noOfQueuesText.setColumns(10);

        noOfQueuesLabel.setBounds(0, 230, 170, 20);
        panel.add(noOfQueuesLabel, c);
        noOfQueuesText.setBounds(0, 255, 100, 20);
        panel.add(noOfQueuesText, c);

//        Simulation interval
        JLabel simulationIntervalLabel = new JLabel("Simulation duration");
        JTextField simulationIntervalText = new JTextField();
        simulationIntervalText.setColumns(10);

        simulationIntervalLabel.setBounds(0, 280, 170, 20);
        panel.add(simulationIntervalLabel, c);
        simulationIntervalText.setBounds(0, 305, 100, 20);
        panel.add(simulationIntervalText, c);

        JButton startButton = new JButton("Start sim.");
        startButton.addActionListener(e -> {
            try {
                minArrivalTime = Integer.parseInt(minArrivingIntervalText.getText());
                maxArrivalTime = Integer.parseInt(maxArrivingIntervalText.getText());
                minServiceTime = Integer.parseInt(minServiceTimeText.getText());
                maxServiceTime = Integer.parseInt(maxServiceTimeText.getText());
                noOfQueues = Integer.parseInt(noOfQueuesText.getText());
                simulationDuration = Integer.parseInt(simulationIntervalText.getText());

                initializeSimulationPanel();
                ClientGenerator clientGenerator = new ClientGenerator();
                clientGenerator.generate(minArrivalTime, maxArrivalTime, minServiceTime, maxServiceTime, noOfQueues, simulationDuration);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
            }
        });

        startButton.setBounds(50, 330, 200, 30);
        panel.add(startButton);
        c.gridx = 0;
        c.gridy = 0;

        panel.repaint();
    }

    private void initializeSimulationPanel() {
        if (simPanel != null) {
            f.remove(simPanel);
        }
        simPanel = new JPanel(null);
        simPanel.setName("simPanel");
        simPanel.setBounds(300, 0, 100 * noOfQueues, 600);
        simPanel.setAlignmentX(JPanel.LEFT_ALIGNMENT);
        simPanel.setAlignmentY(JPanel.TOP_ALIGNMENT);
        JLabel simPanelTitle = new JLabel("Clients simulation");
        simPanelTitle.setBounds(10, 30, 170, 20);
        simPanel.add(simPanelTitle);

        JLabel clientWaitingTimeLabel = new JLabel();
        clientWaitingTimeLabel.setName("clientWaitingTime");
        clientWaitingTimeLabel.setBounds(10, 500, 350, 50);

        simPanel.add(clientWaitingTimeLabel);

        Container pane = f.getContentPane();
        pane.add(simPanel);
        f.setSize(new Dimension(300 + noOfQueues * 150, 600));
        f.repaint();
        java.util.List<JLabel> queueLabels = new LinkedList<>();
        for (int i = 0; i < noOfQueues; i++) {
            JLabel queueLabel = new JLabel("Queue" + (i+1));
            queueLabel.setName("Queue" + (i+1));
            queueLabel.setBounds(100 * i + 30, 50, 200, 20);
            queueLabels.add(queueLabel);
            simPanel.add(queueLabel);
            simPanel.repaint();
        }
    }

    public static void createClient(int clientId) {
        JLabel client = new JLabel(clientId + "");
        client.setName("Client" + clientId);
        client.setBounds(130, 550 - 15 * clientId, 200, 20);
        simPanel.add(client);
        simPanel.revalidate();
        simPanel.repaint();
    }

    private void assignClientToQueue(int clientId, int queueNo) {
        Component[] components = simPanel.getComponents();
        for (Component component : components) {
            if (component instanceof JLabel && component.getName() != null && component.getName().contains("Client" + clientId)) {
                component.setBounds((queueNo+1) * 100 - 40, findLastClientPosition(queueNo) + 15, 200, 20);
                component.setName("Client" + clientId + "_QueueId_" + queueNo);
            }
        }
        simPanel.repaint();
    }

    private int findLastClientPosition(int queueNo) {
        int position = 0;
        Component[] components = simPanel.getComponents();
        for (Component component : components) {
            if (component instanceof JLabel && component.getName() != null && component.getName().contains("QueueId_" + queueNo)) {
                if (component.getY() > position) {
                    position = component.getY();
                }
            }
        }
        if (position < 70) {
            return 70;
        }
        return position;
    }

    /**
     * Metoda care afiseaza fereastra principala
     */
    private static void createAndShowGUI() {
        //Add content to the window.
        f.setLayout(null);
        f.add(new MainWindow());
        f.setSize(new Dimension(300, 600));
        //Display the window.
//        f.pack();
        f.setVisible(true);//making the frame visible
    }

    public static void main(String[] args) {

        //Schedule a job for the event dispatch thread:
        //creating and showing this application's GUI.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                //Turn off metal's use of bold fonts
                UIManager.put("swing.boldMetal", Boolean.FALSE);
                createAndShowGUI();
            }
        });
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Client) {
            Client client = (Client) arg;
            if (client.getStatus().equals(Client.ClientStatus.WAITING_IN_LINE)) {
                assignClientToQueue(client.getId(), client.getQueueId());
            } else if (client.getStatus().equals(Client.ClientStatus.SERVED)) {
                removeClientFromQueue(client, client.getQueueId());
            }
        }
        updateSimulationPanel();
    }

    private void removeClientFromQueue(Client client, int queueId) {
        Component[] components = simPanel.getComponents();
        Component firstClient = findFirstClient(queueId);
        if (firstClient != null) {
            simPanel.remove(firstClient);
            int position = 0;
            JLabel clientWaitingTimeLabel = null;
            for (Component component : components) {
                if (component instanceof JLabel && component.getName() != null && component.getName().contains("QueueId_" + queueId)) {
                    component.setBounds((queueId + 1) * 100 - 40, (position + 1) * 15 + 50, 200, 20);
                    position++;
                } else if (component instanceof JLabel && component.getName() != null && component.getName().contains("clientWaitingTime")) {
                    clientWaitingTimeLabel = (JLabel) component;
                }
            }
            double clientWaitingTime = ((double)client.getLeavingTime().getTime() - (double) client.getArrivalTime().getTime()) / 1000;
            totalWaitingTime += clientWaitingTime;
            removedClients++;

            if (clientWaitingTimeLabel != null) {
                clientWaitingTimeLabel.setText("<html>Clientul " + client.getId() + " a asteptat: " + clientWaitingTime + " secunde. <br/>" +
                        "Timpul mediu de asteptare: " + (totalWaitingTime / removedClients) + "</html>");

                try {
                    Logger.log("Clientul" + client.getId() + " a fost servit, dupa ce a asteptat " + clientWaitingTime + "\n");
                    Logger.log("Timpul mediu de asteptare: " + (totalWaitingTime / removedClients) + "\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            simPanel.repaint();
        }
    }

    private Component findFirstClient(int queueId) {
        Component component = null;
        Component[] components = simPanel.getComponents();
        int position = Integer.MAX_VALUE;
        for (Component c : components) {
            if (c != null && c.getName() != null && c.getName().contains("QueueId_" + queueId) && c.getY() < position) {
                component = c;
                position = c.getY();
            }
        }
        return component;
    }

    private void updateSimulationPanel() {

    }
}
